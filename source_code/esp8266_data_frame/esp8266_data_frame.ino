//==================================================================================================
// esp8266_smart_picture_frame.ino
//
//    embedded source code for the data frame project by stuart patterson
//    data frame uses the waveshare 4.2" e-Paper screen 
//    https://www.waveshare.com/wiki/4.2inch_e-Paper_Module
//    and the esp8266 d1 mini wemos microcontroller https://docs.wemos.cc/en/latest/d1/d1_mini.html
//    wiring of e-paper screen to esp8266
//    BUSY -> D2, RST -> D4, DC -> D3, CS -> D8, CLK -> D5, DIN -> D7, GND -> GND, 3.3V -> 3.3V
//    DO -> /RTS on D1 Mini (it connects its own D0 to its own /RTS line)
//    Add a 3.3K pull-up to D4 e-paper seems to be low on power on pulling the esp8266 into 
//    serial boot mode and a 4.7K pull-down on D8 per the GxEPD2 documentation.
//
//    esp8266 is setup for both deep sleep and Over The Air (OTA) updates of firmware.
//
//    Deep Sleep mode requires RST -> D0 (GPIO 16) RTS goes LOW to case a reset of the microcontroller
//    Since we are using a e-ink/e-paper display, the display does not need power to maintain
//    the display contents.
//
//    This work is licensed under the Creative Commons 
//    Attribution-NonCommercial 4.0 International License. To view a copy
//    of this license, visit http://creativecommons.org/licenses/by-nc/4.0/
//
//    written by: Stuart Patterson 2021
//
//==================================================================================================
#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <ESP8266HTTPClient.h>        //https://links2004.github.io/Arduino/dd/d8d/class_h_t_t_p_client.html
#include <ArduinoJson.h>              // https://arduinojson.org/v6/example/http-client/
#define ENABLE_GxEPD2_GFX 0
#include <GxEPD2_BW.h>
#include <Fonts/FreeMonoBold12pt7b.h>
#define GxEPD2_DISPLAY_CLASS GxEPD2_BW
#define GxEPD2_DRIVER_CLASS GxEPD2_420     // GDEW042T2   400x
#include "GxEPD2_display_selection_new_style.h"
#include "youtube_logo.h"

#define DEBUG                     // define for serial output to contain debugging info
#define SLEEP_TIME 36e8            // in microseconds 30e6 is 30 seconds. 60e6 is 1 minute, 18e8 is 30 minutes, 36e8 is 60 minutes
#define JSON_MEMORY_BUFFER 1024*2 // now much memory to give to the decoding of the json response

void displayStockPrice();
void displayYouTubeSubscriberCount();

// start private data
const char *wifi_ssid = "your_ssid";
const char *wifi_pw = "your_wifi_password";
const char *rapid_api_application_key = "your_rapid_api_application_key";
const char *youtube_api_key = "your_yourtube_api_key";      //https://developers.google.com/youtube/v3/getting-started
const char *youtube_channel_id = "your_youtube_channel_id";                  //https://developers.google.com/youtube/v3/getting-started
// end private data

//--------------------------------------------------------------------------------------------------
// when the esp8266 wakes from a deep sleep, it will do a reset which will cause the setup()
// to be called again
//--------------------------------------------------------------------------------------------------
void setup()
{

  #ifdef DEBUG
    Serial.begin(115200);
    Serial.println("I am awake!");
  #endif

  WiFi.mode(WIFI_STA);
  WiFi.begin(wifi_ssid, wifi_pw);
  
  while (WiFi.status() != WL_CONNECTED) {
    delay(1000);
  }

  #ifdef DEBUG
    Serial.print("Connect to WIFI using IP: ");
    Serial.println(WiFi.localIP());
  #endif
  
  display.init(115200);
  display.setRotation(3);     // portrait mode
}

//--------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------
void loop()
{
  //displayStockPrices();
  displayYouTubeSubscriberCount();
  
  display.hibernate();
  ESP.deepSleep(SLEEP_TIME);
}

//--------------------------------------------------------------------------------------------------
//  displayStockPrices()
//    gets the latest stock price from the local stock_symbols array.  It would be nice to show
//    last vs current so you could see if the stock is going up or down
//--------------------------------------------------------------------------------------------------
void displayStockPrices()
{
  const int json_memory_buffer = 1024*2;
  char *stock_symbols[] = { "AAPL","MSFT", "ORCL", "TSLA" } ; 
  int stock_symbols_count = sizeof(stock_symbols)/sizeof(char *);
  float stock_prices[stock_symbols_count];          // can be determined at compile time!
  
  WiFiClientSecure client;
  client.setInsecure();      // as long as it is SSL we are good, not checking actual ssl key
  HTTPClient https;
  https.useHTTP10(true);    // required for deserializeJson (https://arduinojson.org/v6/how-to/use-arduinojson-with-httpclient/)

  #ifdef DEBUG 
    Serial.print("Stock Symbols count = ");
    Serial.println(stock_symbols_count);
  #endif

  String restful_url = "https://stock-price4.p.rapidapi.com/price/%stock_symbol%";
   
  for(int index=0; index < stock_symbols_count; ++index) {
    String url = restful_url;
    url.replace("%stock_symbol%",stock_symbols[index]);
    
    #ifdef DEBUG 
      Serial.println("[HTTPS] begin...");
      Serial.println(url);
    #endif

    if ( https.begin(client, url.c_str() )) {  
      https.addHeader("x-rapidapi-key",rapid_api_application_key);
      https.addHeader("x-rapidapi-host","stock-price4.p.rapidapi.com");
  
      int httpCode = https.GET();
      #ifdef DEBUG 
        Serial.printf("[HTTPS] GET... code: %d\n", httpCode);
      #endif
     
      if (httpCode > 0) {
        if (httpCode == 200 ) {
          DynamicJsonDocument doc(JSON_MEMORY_BUFFER);
          DeserializationError error = deserializeJson(doc, https.getStream());
          if (error) {
            #ifdef DEBUG
                Serial.print("deserializeJson() failed: ");
                Serial.println(error.f_str());
            #endif
            stock_prices[index] = -1;
          }
          else {
            stock_prices[index] = doc["Price"].as<float>();
         }
       }
     } 
    }
  }
  
  https.end();
  
  char display_line[80];
  int16_t tbx, tby; uint16_t tbw, tbh; // boundary box window
  uint16_t x, y;
  y = 100;    // starting line of stock data on screen, x pos will be calculated for center

  display.fillScreen(GxEPD_WHITE);
  display.setTextColor(GxEPD_BLACK);
  display.setFont(&FreeMonoBold12pt7b);

  display.setCursor(100,50);
  display.print("NASDAQ");

  // now lets display the stock prices
  for(int index=0; index < stock_symbols_count; ++index) {
    sprintf(display_line, "%s $%.2Lf", stock_symbols[index], stock_prices[index]);
    display.getTextBounds(display_line, 0, 0, &tbx, &tby, &tbw, &tbh); // it works for origin 0, 0, fortunately (negative tby!)
    x = ((display.width() - tbw) / 2) - tbx;
    display.setCursor(x, y); // set the postition to start printing text
    display.print(display_line); // print some text
    y += (tbh+4);
  }
  display.display();
}

//--------------------------------------------------------------------------------------------------
// displayYouTubeSubscriberCount()
//    Displays subscriber count, total number of public videos, and viewer count
//    using the globally defined youtube_channel_id and youtube_api_key vars
//--------------------------------------------------------------------------------------------------
void displayYouTubeSubscriberCount()
{
  const int json_memory_buffer = 1024*2;
  int youtube_subscriber_count;
  int youtube_view_count;
  int youtube_video_count;
  
  WiFiClientSecure client;
  client.setInsecure();      // as long as it is SSL we are good, not checking actual ssl key
  HTTPClient https;
  https.useHTTP10(true);    // required for deserializeJson (https://arduinojson.org/v6/how-to/use-arduinojson-with-httpclient/)

  String restful_url = "https://www.googleapis.com/youtube/v3/channels?part=statistics&id=%youtube_channel_id%&key=%api_key%";
  
  String url = restful_url;
  url.replace("%youtube_channel_id%",youtube_channel_id);
  url.replace("%api_key%",youtube_api_key);

  #ifdef DEBUG 
        Serial.println("displayYouTubeSubscriberCount()");
        Serial.print("calling url: ");
        Serial.println(url);
  #endif

  if ( https.begin(client, url.c_str() )) {  
      int httpCode = https.GET();
      #ifdef DEBUG 
        Serial.printf("[HTTPS] GET... code: %d\n", httpCode);
      #endif
     
      if (httpCode > 0) {
        if (httpCode == 200 ) {
          DynamicJsonDocument doc(JSON_MEMORY_BUFFER);
          DeserializationError error = deserializeJson(doc, https.getStream());
          if (error) {
            #ifdef DEBUG
                Serial.print("deserializeJson() failed: ");
                Serial.println(error.f_str());
            #endif
            youtube_subscriber_count = -1;
            youtube_view_count = -1;
            youtube_video_count = -1;
          }
          else {
            #ifdef DEBUG
                Serial.println(https.getString());
            #endif
            youtube_subscriber_count = doc["items"][0]["statistics"]["subscriberCount"].as<int>();
            youtube_view_count = doc["items"][0]["statistics"]["viewCount"].as<int>();
            youtube_video_count = doc["items"][0]["statistics"]["videoCount"].as<int>();
         }
       }
     } 
    }
  
  https.end();

  char display_line[80];
  int youtube_logo_width = 250;
  int youtube_logo_height = 56;
  int horizontal_center = (300-youtube_logo_width)/2;
  int starting_y_for_text = 150;
  int vertical_spacing = 4;
  int line_number = 0;
  int16_t tbx, tby; uint16_t tbw, tbh; // boundary box window
  uint16_t x, y;

  display.setTextColor(GxEPD_BLACK);
  display.setFont(&FreeMonoBold12pt7b);

  // youtube logo graphic
  display.fillScreen(GxEPD_WHITE);
  display.drawBitmap(horizontal_center, 50, youtube_logo, youtube_logo_width, youtube_logo_height, GxEPD_BLACK);

  // subscribers
  sprintf(display_line, "Subscribers");
  display.getTextBounds(display_line, 0, 0, &tbx, &tby, &tbw, &tbh); // it works for origin 0, 0, fortunately (negative tby!)
  x = ((display.width() - tbw) / 2) - tbx;
  y = starting_y_for_text;
  display.setCursor(x, y); // set the postition to start printing text
  display.print(display_line); // print some text
  ++line_number;
  
  sprintf(display_line, "%d", youtube_subscriber_count);
  display.getTextBounds(display_line, 0, 0, &tbx, &tby, &tbw, &tbh); // it works for origin 0, 0, fortunately (negative tby!)
  x = ((display.width() - tbw) / 2) - tbx;
  y = starting_y_for_text+((tbh+vertical_spacing)*line_number);    // tbh height of font + a few pixels
  display.setCursor(x, y); // set the postition to start printing text
  display.print(display_line); // print some text
  line_number += 4;

   // view count
  sprintf(display_line, "View Count");
  display.getTextBounds(display_line, 0, 0, &tbx, &tby, &tbw, &tbh); // it works for origin 0, 0, fortunately (negative tby!)
  x = ((display.width() - tbw) / 2) - tbx;
  y = starting_y_for_text+((tbh+vertical_spacing)*line_number);
  display.setCursor(x, y); // set the postition to start printing text
  display.print(display_line); // print some text
  ++line_number;
  
  sprintf(display_line, "%d", youtube_view_count);
  display.getTextBounds(display_line, 0, 0, &tbx, &tby, &tbw, &tbh); // it works for origin 0, 0, fortunately (negative tby!)
  x = ((display.width() - tbw) / 2) - tbx;
  y = starting_y_for_text+((tbh+vertical_spacing)*line_number);    // tbh height of font + a few pixels
  display.setCursor(x, y); // set the postition to start printing text
  display.print(display_line); // print some text
  line_number += 4;

   // video count
  sprintf(display_line, "Video Count");
  display.getTextBounds(display_line, 0, 0, &tbx, &tby, &tbw, &tbh); // it works for origin 0, 0, fortunately (negative tby!)
  x = ((display.width() - tbw) / 2) - tbx;
  y = starting_y_for_text+((tbh+vertical_spacing)*line_number);
  display.setCursor(x, y); // set the postition to start printing text
  display.print(display_line); // print some text
  ++line_number;
  
  sprintf(display_line, "%d", youtube_video_count);
  display.getTextBounds(display_line, 0, 0, &tbx, &tby, &tbw, &tbh); // it works for origin 0, 0, fortunately (negative tby!)
  x = ((display.width() - tbw) / 2) - tbx;
  y = starting_y_for_text+((tbh+vertical_spacing)*line_number);    // tbh height of font + a few pixels
  display.setCursor(x, y); // set the postition to start printing text
  display.print(display_line); // print some text
  ++line_number;
  
  
  display.display(); 
}
