EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L MCU_Module:WeMos_D1_mini U1
U 1 1 60CE1646
P 3600 3250
F 0 "U1" H 3600 2361 50  0000 C CNN
F 1 "WeMos_D1_mini" H 3600 2270 50  0000 C CNN
F 2 "Module:WEMOS_D1_mini_light" H 3600 2100 50  0001 C CNN
F 3 "https://wiki.wemos.cc/products:d1:d1_mini#documentation" H 1750 2100 50  0001 C CNN
	1    3600 3250
	1    0    0    -1  
$EndComp
$Comp
L epaper:4_2_E-Paper DSP1
U 1 1 60CE59D0
P 5050 3100
F 0 "DSP1" H 5478 3101 50  0000 L CNN
F 1 "4_2_E-Paper" H 5478 3010 50  0000 L CNN
F 2 "" H 4900 2900 50  0001 C CNN
F 3 "" H 4900 2900 50  0001 C CNN
	1    5050 3100
	1    0    0    -1  
$EndComp
Wire Wire Line
	4550 2450 4550 2800
Wire Wire Line
	3600 4050 4400 4050
Wire Wire Line
	4400 4050 4400 4000
Wire Wire Line
	4400 2900 4550 2900
Wire Wire Line
	4000 3650 4450 3650
Wire Wire Line
	4450 3650 4450 3200
Wire Wire Line
	4450 3200 4550 3200
Wire Wire Line
	4550 3000 4350 3000
Wire Wire Line
	4350 3000 4350 3550
Wire Wire Line
	4350 3550 4000 3550
Wire Wire Line
	4000 3050 4100 3050
Wire Wire Line
	4100 3050 4100 3500
Wire Wire Line
	4100 3500 4500 3500
Wire Wire Line
	4000 3150 4200 3150
Wire Wire Line
	4200 3150 4200 3300
Wire Wire Line
	4200 3300 4550 3300
Wire Wire Line
	4000 3350 4050 3350
Wire Wire Line
	4050 3350 4050 3100
Wire Wire Line
	4050 3100 4550 3100
Text Notes 4900 3500 0    50   ~ 0
Per GxEPD Lib\nBUSY -> D2\nRST -> D4\nDC -> D3\nCS -> D8\nCLK -> D5\nDIN -> D7\nGND -> GND\n3.3V -> 3.3V
$Comp
L Device:R R1
U 1 1 60CEA845
P 4250 2700
F 0 "R1" H 4320 2746 50  0000 L CNN
F 1 "3.3K" H 4320 2655 50  0000 L CNN
F 2 "" V 4180 2700 50  0001 C CNN
F 3 "~" H 4250 2700 50  0001 C CNN
	1    4250 2700
	1    0    0    -1  
$EndComp
Wire Wire Line
	4000 3250 4150 3250
Wire Wire Line
	4150 3250 4150 3400
Wire Wire Line
	4250 3400 4250 2850
Wire Wire Line
	4150 3400 4250 3400
Connection ~ 4250 3400
Wire Wire Line
	4250 3400 4550 3400
Wire Wire Line
	3200 2350 3200 2850
Wire Wire Line
	4100 2350 4100 2850
Wire Wire Line
	4100 2850 4000 2850
Wire Wire Line
	3200 2350 4100 2350
Text Notes 3200 2300 0    50   ~ 0
Deep Sleep wake up on D0 -> /RST
NoConn ~ 3200 3150
NoConn ~ 3200 3250
$Comp
L power:+5V #PWR01
U 1 1 60CEE60B
P 3000 2450
F 0 "#PWR01" H 3000 2300 50  0001 C CNN
F 1 "+5V" V 3015 2578 50  0000 L CNN
F 2 "" H 3000 2450 50  0001 C CNN
F 3 "" H 3000 2450 50  0001 C CNN
	1    3000 2450
	0    -1   -1   0   
$EndComp
Text Notes 2700 4650 0    50   ~ 0
D8 (GPIO 15)  D3 (GPIO 0)  D4 (GPIO 2)\n        0            0           1           Program \n        0            1           1           Boot from flash\n
Wire Wire Line
	3000 2450 3100 2450
Wire Wire Line
	3700 2450 4550 2450
Wire Wire Line
	3100 2450 3100 2100
Wire Wire Line
	3100 2100 4250 2100
Wire Wire Line
	4250 2100 4250 2550
Connection ~ 3100 2450
Wire Wire Line
	3100 2450 3500 2450
Text Notes 1600 3600 0    50   ~ 0
If programming via USB and Arduino IDE \ndoes not find board, press reset button \non D1 mini when IDE is "Connecting..."
$Comp
L Device:R R2
U 1 1 60CF6458
P 4700 3850
F 0 "R2" H 4770 3896 50  0000 L CNN
F 1 "4.7K" H 4770 3805 50  0000 L CNN
F 2 "" V 4630 3850 50  0001 C CNN
F 3 "~" H 4700 3850 50  0001 C CNN
	1    4700 3850
	1    0    0    -1  
$EndComp
Wire Wire Line
	4700 3700 4500 3700
Wire Wire Line
	4500 3700 4500 3500
Connection ~ 4500 3500
Wire Wire Line
	4500 3500 4550 3500
Wire Wire Line
	4700 4000 4400 4000
Connection ~ 4400 4000
Wire Wire Line
	4400 4000 4400 2900
$EndSCHEMATC
